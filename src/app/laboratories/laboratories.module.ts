import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {LaboratoriesRoutingModule} from './laboratories-routing.module';
import {LaboratoriesComponent} from './laboratories.component';
import {SecondLaboratoryComponent} from './pages/second-laboratory/second-laboratory.component';
import {ThirdLaboratoryComponent} from './pages/third-laboratory/third-laboratory.component';
import {FourthLaboratoryComponent} from './pages/fourth-laboratory/fourth-laboratory.component';
import {FifthLaboratoryComponent} from './pages/fifth-laboratory/fifth-laboratory.component';
import {SixthLaboratoryComponent} from './pages/sixth-laboratory/sixth-laboratory.component';
import {SeventhLaboratoryComponent} from './pages/seventh-laboratory/seventh-laboratory.component';
import {EighthLaboratoryComponent} from './pages/eighth-laboratory/eighth-laboratory.component';
import {FirstLaboratoryComponent} from "./pages/first-laboratory/first-laboratory.component";
import {NzTypographyModule} from "ng-zorro-antd/typography";


@NgModule({
    declarations: [
        LaboratoriesComponent,
        FirstLaboratoryComponent,
        SecondLaboratoryComponent,
        ThirdLaboratoryComponent,
        FourthLaboratoryComponent,
        FifthLaboratoryComponent,
        SixthLaboratoryComponent,
        SeventhLaboratoryComponent,
        EighthLaboratoryComponent
    ],
    imports: [
        CommonModule,
        LaboratoriesRoutingModule,
        NzTypographyModule
    ]
})
export class LaboratoriesModule {
}
