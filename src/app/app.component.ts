import {Component, OnInit} from '@angular/core';
import {MenuItems} from "./menu-items";
import {ThemeService} from "./theme.service";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {
  static LANG_KEY = 'kspi_lang';
  RU = 'ru';
  UZ = 'uz';
  EN = 'en';

  currentLang = this.UZ;

  isCollapsed = false;

  menu = MenuItems.items

  constructor(private themeService: ThemeService, private translate: TranslateService) {}

  ngOnInit() {
    let savedTheme = localStorage.getItem(ThemeService.THEME_KEY);
    this.themeService.currentTheme = savedTheme === null ? ThemeService.DEFAULT : savedTheme;
    this.themeService.loadTheme(true).finally();

    let savedLang = localStorage.getItem(AppComponent.LANG_KEY);
    if (savedLang === null) {
      savedLang = this.UZ;
    }
    this.currentLang = savedLang;
    this.toggleLang(savedLang);
  }

  toggleTheme() {
    this.themeService.toggleTheme().then();
  }

  toggleLang(lang: string) {
    this.translate.use(lang);
    localStorage.setItem(AppComponent.LANG_KEY, lang);

  }

  onToggleLang() {
    this.toggleLang(this.currentLang);
  }
}
